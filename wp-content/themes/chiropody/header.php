<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="profile" href="https://gmpg.org/xfn/11"/>
    <?php wp_head(); ?>
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_site_url(); ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_site_url(); ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="194x194" href="<?php echo get_site_url(); ?>/favicon-194x194.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo get_site_url(); ?>/android-chrome-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_site_url(); ?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_site_url(); ?>/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#009bda">
    <meta name="msapplication-TileColor" content="#009bda">
    <meta name="msapplication-TileImage" content="<?php echo get_site_url(); ?>/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<?php
$header_logo = get_field('header_logo', 'option');
if ($header_logo) {
    $logo_src = $header_logo;
} else {
    $logo_src = get_bloginfo('stylesheet_directory') . '/images/logo.png';
} 
$header_inner_logo = get_field('header_inner_page_logo', 'option');
if ($header_inner_logo) {
    $inner_logo_src = $header_inner_logo;
} else {
    $inner_logo_src = get_bloginfo('stylesheet_directory') . '/images/innerlogo.png';
} 
$phone_number = get_field('phone_number', 'option');
$email_address = get_field('email_address', 'option');
?>
<div class="site">
    <header class="site-header">
        <div class="site-header-main-wrap">
            <div class="container">
                <div class="site-header-main">
                    <?php if (is_front_page() ) { ?>
                     <?php echo do_shortcode('[social]');?>
                   <?php }else{ ?>
                     <div class='has-contact-btn'>
                           <?php if( $phone_number ){ ?>
                                 <a href="skype:<?php echo $phone_number ;?>" class="disable-link">
                                      <i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone_number ;?>
                                 </a>
                            <?php } ?>
                            <?php if( $email_address ){ ?>
                                   <a href="mailto:<?php echo $email_address;?>">
                                           <i class="fa fa-envelope-o" aria-hidden="true"></i> EMAIL US
                                         </a>
                                      <?php } ?>
                                  </div>
                   <?php } ?>
                    <button class="menu-toggle burger">
                        <span>&nbsp;</span>
                    </button>
                    <nav class="main-navigation">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'header',
                            'menu_class' => 'primary-menu',
                        ));
                        ?>
                    </nav>
                    <div class="overlay">&nbsp;</div>


                </div>
            </div>
        </div>
       <?php if (is_front_page() ) { ?>
	   <?php include 'banner.php'; ?>
       <?php }else{ ?>
         <div class="site-banner">
           <div class="container">
            <a class='site-logo' href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                 <img src="<?php echo $header_inner_logo; ?>" alt="<?php echo get_bloginfo( 'name' ); ?>">
            </a>
          </div>
         </div>
       <?php } ?>
    </header>
    <div class="site-content">
