<?php
/*
*
* Template Name: News Page
*
*
*/
get_header();
?>
   <div class="content-area">
        <?php if (have_posts()) { ?>
            <?php while (have_posts()) {
                the_post(); ?>
                <div class="site-section pt-0 text-center">
                    <div class="container">
                     <h1 class="entry-title"><?php echo get_the_title();?></h1>
                              <?php the_content();?>

                    </div>
		        </div>

                <div class="site-section pt-0">
                    <div class="container">
                              <?php 
                                 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		                 $args = array(
				    'post_type' => 'post',
				   'posts_per_page' => 3,
                                   'paged'         => $paged,
		                 );
		                $news = new WP_Query($args);
                                $newscount = $news->found_posts;
                               if ($news->have_posts()): ?>
                                <div class="news-list">
		                <?php while ($news->have_posts()): $news->the_post(); 
                                  $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'full' ); 
                                  $page_link = get_field('page_link');
                                  $post_date = get_the_date( ' j F, Y' );
                                  $post_id[] = get_the_ID();
                                  ?>
                                   <div class="news-item">
                                          <div class="news-item-thumb">
                                             <?php if( $image_url ){ ?>
                                                <img src="<?php echo $image_url;?>" alt="<?php echo get_the_title();?>" />
                                              <?php } ?>
                                          </div>
                                        <div class="news-item-content">
                                          <h3 class="news-item-title"><?php echo get_the_title();?></h3>
                                          <span class="time"><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $post_date; ?></span>
                                          <?php the_content();?>
                                         <?php if( $page_link ){ ?>
                                          <a class='btn btn-primary rounded-0' href="<?php echo $page_link;?>">Read More</a>
                                         <?php } ?>
                                      </div>
                                       
                                   </div>
                                <?php endwhile;
                                wp_reset_postdata(); 
                                $post_ids = implode(',',$post_id );
                                ?>
                              </div>
                             <?php
                                if (  $newscount > 1 )
	                             echo '<a href="javascript:void(0)" class="misha_loadmore">Load More</a>'; // you can use <a> as well
                              ?>
		             <?php endif; ?>
                          </div>
                    </div>
		</div>
            <?php } ?>
        <?php } ?>
<?php include 'page-bottom.php'; ?>
    </div>

<?php get_footer();
