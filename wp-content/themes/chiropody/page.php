<?php

include "header.php";
echo '<div class="content-area">';
 echo '<div class="container">'; ?>
   <div class="site-section pt-0">
      <div class=" text-center">
        <h1 class="entry-title"><?php echo get_the_title();?></h1>
      </div>
        <?php  
        while (have_posts()) :
            the_post(); ?>
            <?php the_content(); ?>		
        <?php endwhile; ?>

   </div>
<?php 
 echo '</div>';
echo '</div>';

include "footer.php";

?>


