<?php
/*
*
* Template Name: Home Page
*
*
*/
get_header();
$video_image = get_field('video_image','option');
$youtube_video_id = get_field('youtube_video_id','option');
?>

    <div class="content-area">
        <?php if (have_posts()) { ?>

            <?php while (have_posts()) {
                the_post(); ?>

             <div class="container">
		       <?php if( have_rows('page_section') ): ?>	   
                            <?php while ( have_rows('page_section') ) : the_row(); ?>
						<?php if( get_row_layout() == 'page_heading' ):
							$title = get_sub_field('page_heading');
							$content = get_sub_field('content');
							?>
							<div class="site-section text-center py-0">
								<div class="container">
								   <h1 class="entry-title"><?php echo $title;?></h1>
								</div>
							</div>
						<?php elseif( get_row_layout() == 'content_section' ): 
							$content = get_sub_field('content');
						?>
                                                    <div class="site-section text-center pt-0">
								<div class="container">
								   <p class='px-lg-5'><?php echo $content;?></p>
								</div>
							</div>
                                                 <?php elseif( get_row_layout() == 'logo_icon_section' ): 
							$upload_logo = get_sub_field('upload_logo');
                                                        if( $upload_logo ){
						?>
                                                    <div class="site-section text-center pt-0">
							 <div class="container">
								<img src="<?php echo $upload_logo;?>" alt="" />
							   </div>
							</div>
                                                     <?php } ?>
                                                 <?php elseif( get_row_layout() == 'page_grid_section' ): 
							if( have_rows('page_grid_section') ):
						?>
						   <div class="site-section pt-0">
							<div class="container">
									 <div class="row">
									  <?php while( have_rows('page_grid_section') ) : the_row();
										$upload_image = get_sub_field('upload_image');
										$title = get_sub_field('title');
                                                                                $page_link = get_sub_field('page_link');
										$page_description = get_sub_field('page_description'); ?>
										   <div class="col-12 col-md-6 col-lg-3">
										     <div class="service-card">
											  <?php if( $upload_image ){ ?>
											      <div class="service-thumb">
												 <img src="<?php echo $upload_image;?>" alt="" />
											      </div>
											   <?php } ?>

                                                 <div class="service-card-content">
                                                    <h4 class="service-card-title">
                                                        <a href="<?php echo $page_link;?>"><?php echo $title;?></a>
                                                     </h4>
                                                    <a class='btn btn-primary' href="<?php echo $page_link;?>">Find out more</a>
                                                 </div>

                                                 <div class="service-card-hover-content">
                                                    <h4 class="service-card-title"><a href="<?php echo $page_link;?>"><?php echo $title;?> </a></h4>
                                                    <a href="<?php echo $page_link;?>"><?php echo $page_description;?></a>
                                                 </div>
											 </div>
										  </div>
										 <?php endwhile; ?>
									  </div>
							      </div>
						       </div>
						    <?php endif; ?>
                                              <?php elseif( get_row_layout() == 'email_phone_number_section' ): 
							$show_email_address = get_sub_field('show_email_address');  
                                                        $show_phone_number = get_sub_field('show_phone_number');
                                                        $phone_number = get_field('phone_number', 'option');
                                                        $email_address = get_field('email_address', 'option');
                                                        if( $upload_logo ){
						    ?>
                            <div class="site-section text-center pt-0">
							   <div class="container">
                                  <div class='has-contact-btn'>
								  <?php if( $show_email_address[0] == 'Yes' && $phone_number ){ ?>
                                         <a href="skype:<?php echo $phone_number ;?>" class="disable-link">
                                          <i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone_number ;?></a>
                                       <?php } ?>
                                       <?php if( $show_phone_number[0] == 'Yes' && $email_address ){ ?>
                                         <a href="mailto:<?php echo $email_address;?>"><i class="fa fa-envelope-o" aria-hidden="true"></i> Email Us</a>
                                      <?php } ?>
                                   </div>
							   </div>
							</div>
                                 <?php } ?>
                             <?php endif; ?>
                           <?php endwhile;?>
                       <?php endif; ?>
                    </div>
            <?php } ?>

        <?php } ?>
    </div>
<?php include 'page-bottom.php'; ?>
<?php get_footer();
