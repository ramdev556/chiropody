<?php
if ( ! function_exists( 'chiropody_setup' ) ) :

    function chiropody_setup() {

        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'title-tag' );

        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 1600, 600 );

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(
            array(
                'header' => __( 'Header', 'chiropody' ),
                'footer' => __( 'Footer', 'chiropody' ),
                'Social' => __( 'Social', 'chiropody' ),
            )
        );

        add_theme_support(
            'html5',
            array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );

        add_theme_support( 'customize-selective-refresh-widgets' );
        add_theme_support( 'wp-block-styles' );
        add_theme_support( 'editor-styles' );
        add_theme_support( 'responsive-embeds' );
    }
endif;
add_action( 'after_setup_theme', 'chiropody_setup' );

function chiropody_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'chiropody_content_width', 767 );
}
add_action( 'after_setup_theme', 'chiropody_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function chiropody_scripts() {
    wp_enqueue_script('jquery-bootstrap', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20190415', true);
    wp_enqueue_script('owl-carousel-jquery', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '20190415', true);
    wp_enqueue_script('theme-custom-script', get_stylesheet_directory_uri() . '/js/theme-custom-script.js', array('jquery'), '20190415', true);
    

	wp_enqueue_style('owl-carousel-style', get_stylesheet_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style('owl-default-style', get_stylesheet_directory_uri() . '/css/owl.theme.default.css');
    wp_enqueue_style('font-awesome-min', get_stylesheet_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style('bootstrap-min', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css');
}
add_action( 'wp_enqueue_scripts', 'chiropody_scripts' );

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme Options',
        'menu_title' => 'Theme Options',
        'menu_slug' => 'ls-theme-options',
        'capability' => 'manage_options',
        'redirect' => false
    ));

    acf_add_options_sub_page(array(
        'page_title' => 'Social Links',
        'menu_title' => 'Social Links',
        'parent_slug' => 'ls-theme-options'
    ));

}


function social_fun($socialContent) {
    $facebook_link = get_field('facebook_link', 'option');
    $twitter_link = get_field('twitter_link', 'option');
	$instagram_link = get_field('instagram_link', 'option');
    $socialContent = '<ul class="social-link">';

    if ( $facebook_link ) {
        $socialContent .= '<li><a  target="_blank" href="' . $facebook_link . '"><i class="fa fa-facebook"></i></a></li>';
    }

    if ( $twitter_link ) {
        $socialContent .= '<li><a target="_blank" href="' . $twitter_link . '"><i class="fa fa-twitter"></i></a></li>';
    }

	if ( $instagram_link ) {
        $socialContent .= '<li><a target="_blank" href="' . $instagram_link . '"><i class="fa fa-instagram"></i></a></li>';
    }

    $socialContent .= '</ul>';
    return $socialContent;
}

add_shortcode('social', 'social_fun', 99);


add_filter('login_headerurl', 'custom_loginlogo_url');

function custom_loginlogo_url($url)
{
    return get_site_url();
}

function custom_login_logo()
{
    echo "
	<style>
		body.login #login h1 a {
		background: url('" . get_bloginfo('stylesheet_directory') . "/images/logo.png') 10px 0 no-repeat transparent;
		height:100px;
		width:280px; }
	</style>
	";
}

add_action('login_head', 'custom_login_logo');
function admin_login_design()
{
    ?>
    <style type="text/css">
        .login-action-login {
            background-color: #b01b1f;
        }

        .login-action-login .login form {
            background-color: #131254
        }

        #loginform {
            background-color: #131254;
        }

        .login label {
            font-size: 12px;
            color: #fff !important;
        }

        .login .button-primary {
            width: 120px;
            float: right;
            background-color: #fff !important;
            color: #fff !important;
            -webkit-border-radius: 4px;
            border: 1px solid #fff !important;
            text-shadow: none !important;
        }
        
        .login #backtoblog a, .login #nav a {
    color: #ffffff !important;
}

      .login .button-primary {
         color: #000 !important;
      }
    </style>
    <?php
}

add_action('login_enqueue_scripts', 'admin_login_design');

function remove_editor() {
    if (isset($_GET['post'])) {
        $id = $_GET['post'];
        $template = get_post_meta($id, '_wp_page_template', true);
        switch ($template) {
            case 'page-home.php':
            remove_post_type_support('page', 'editor');
            break;
            default :
            // Don't remove any other template.
            break;
        }
    }
}
add_action('init', 'remove_editor');



function misha_my_load_more_scripts() {
 
	global $wp_query; 
 
	wp_enqueue_script('jquery');
 
	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/js/myloadmore.js', array('jquery') );
 
	wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $wp_query->max_num_pages
	) );
 
 	wp_enqueue_script( 'my_loadmore' );
}
 
add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );


function misha_loadmore_ajax_handler(){
        
	// prepare our arguments for the query
	//$args = json_decode( stripslashes( $_POST['query'] ), true );
        //$args['post_type'] = 'post';
	//$args['paged'] = $_POST['page'] + 1; 
	//$args['post_status'] = 'publish';
        $args = array(
	  'post_type' => 'post',
	  'posts_per_page' => 3,
          'paged'         => $_POST['page'] + 1,
        );
	$news = new WP_Query($args);
        //echo '<pre>'; print_r( $args ); ?>
	<div class="news-list">
		                <?php while ($news->have_posts()): $news->the_post(); 
                                  $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'full' ); 
                                  $page_link = get_field('page_link');
                                  $post_date = get_the_date( ' j, F Y' );
                                  $post_id[] = get_the_ID();
                                  ?>
                                   <div class="news-item">
                                          <div class="news-item-thumb">
                                             <?php if( $image_url ){ ?>
                                                <img src="<?php echo $image_url;?>" alt="<?php echo get_the_title();?>" />
                                              <?php } ?>
                                          </div>
                                        <div class="news-item-content">
                                          <h3 class="news-item-title"><?php echo get_the_title();?></h3>
                                          <span class="time"><i class="fa fa-clock-o" aria-hidden="true"></i><?php echo $post_date; ?></span>
                                          <?php the_content();?>
                                         <?php if( $page_link ){ ?>
                                          <a class='btn btn-primary rounded-0' href="<?php echo $page_link;?>">Read More</a>
                                         <?php } ?>
                                      </div>
                                       
                                   </div>
                                <?php endwhile;
                                wp_reset_postdata(); 
                                ?>
                              </div>
    <?php die; 
}
 
 
 
add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}