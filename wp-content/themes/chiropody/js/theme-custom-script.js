jQuery(document).ready(function() {

    jQuery(function () {
        var header = jQuery(".site-header");

        jQuery(window).scroll(function () {
            var scroll = jQuery(window).scrollTop();
            if (scroll >= 1) {
                header.addClass("is-sticky-header");
            } else {
                header.removeClass("is-sticky-header");
            }
        });

    });

    if (jQuery(window).width() < 992) {
        jQuery('.burger, .overlay').click(function(){
            jQuery('.burger').toggleClass('clicked');
            jQuery('.overlay').toggleClass('show');
            jQuery('nav').toggleClass('show');
            jQuery('body').toggleClass('overflow');
        });
    }

    // jQuery('select').select2();


});
