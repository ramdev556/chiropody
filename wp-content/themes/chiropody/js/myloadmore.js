jQuery(function($){ 
	jQuery('.misha_loadmore').click(function(){
		var button = $(this),
		    data = {
			'action': 'loadmore',
			'query': misha_loadmore_params.posts, 
			'page' : misha_loadmore_params.current_page
		};
 
		$.ajax({ // you can also use $.post here
			url : misha_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); // change the button text, you can also add a preloader image
			},
			success : function( data ){
				if( data ) { 
					button.text( 'Load More' ).prev().after(data); 
					misha_loadmore_params.current_page++;
					if ( misha_loadmore_params.current_page == misha_loadmore_params.max_page ) 
						button.remove(); 
				} else {
					button.remove(); 
				}
			}
		});
	});
});