<?php
 $recommended_provider_heading = get_field('recommended_provider_heading');
$recommended_provider_description = get_field('recommended_provider_description');
$recommended_provider_upload_logo = get_field('recommended_provider_upload_logo');
$website_link = get_field('website_link');
?>    
<div class="site-section text-center border-tb-section">

   <div class="container">

     <h2 class='sec-title mb-lg-5'><?php echo $recommended_provider_heading;?></h2>
     <div class='mb-3 mb-lg-5'>
        <img src="<?php echo $recommended_provider_upload_logo;?>" alt="" />
     </div>

     <p><?php echo $recommended_provider_description;?></p>
     <p class="has-contact">Call <a class="disable-link" href="skype:07703608294">07703608294</a> or email <a href="mailto:lisa@shoemed.co.uk">lisa@shoemed.co.uk</a> to schedule your time & date. </p>

    <?php if( have_rows('recommended_provider_images') ): ?>  
    <div class='shoes-view'>

        <?php while( have_rows('recommended_provider_images') ) : the_row(); 
           $upload_image = get_sub_field('upload_image');
         ?>
           <img src="<?php echo $upload_image;?>" alt="" />
        <?php endwhile;?>

     </div>
    <?php endif;?>
   <?php if( $website_link ){ ?>
     <div class="has-contact-btn"><a href="<?php echo $website_link;?>">Visit Website</a></div>
   <?php } ?>

   </div>

  </div>                         
 

	

  <div class="site-section text-center">

   <div class="container">

     <h2 class='sec-title'>Contact</h2>

     <p class='mb-lg-5'>For any enquiries or questions about our services or to book please use the form below.</p>

     <div class='max-w800'>

        <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]');?>

     </div>

   </div>

  </div>