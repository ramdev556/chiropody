<?php
include "header.php";
?>
 <div class="content-area">
	  <div class="site-section pt-0 text-center">
	   <div class="container">
	    <div class="row">
			 <div class="col-12">
                <div class="mb-3 mb-lg-10">
                    <h2 class="display-2 mb-2">404</h2>
                    <div class="icon-not-found mx-auto">
                        <i class="fa fa-exclamation-triangle"></i>
                    </div>
                </div>
				<h5>UH OH! Looks like somethings gone wrong.</h5>
				<a class="btn btn-primary mt-xl-5" href="<?php echo get_site_url();?>" >Go Home</a>
            </div>
		  </div>
		</div>
	  </div>

  </div>
<?php
include "footer.php";


