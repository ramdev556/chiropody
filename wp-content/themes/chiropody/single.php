<?php

include "header.php";
echo '<div class="content-area">'; ?>

<div class="site-section">
    <div class="container">
        <?php echo '<h1 class="entry-title">' . get_the_title() . '</h1>';
        while (have_posts()) :
            the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    </div>
</div>

<?php echo '</div>';

include "footer.php";

?>


