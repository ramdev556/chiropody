<?php
/*
*
* Template Name: Service Page
*
*
*/
get_header();
?>
   <div class="content-area">
        <?php if (have_posts()) { ?>
            <?php while (have_posts()) {
                the_post(); ?>
                <div class="site-section pt-0 text-center">
                    <div class="container">
                     <h1 class="entry-title"><?php echo get_the_title();?></h1>
                              <?php the_content();?>

                    </div>
		        </div>

                <div class="site-section pt-0">
                    <div class="container">
                              <?php 
		                 $args = array(
				    'post_type' => 'services',
				   'posts_per_page' => -1,
		                 );
		                $services = new WP_Query($args);
                               if ($services->have_posts()): ?>
                                <div class="service-list">
		                <?php while ($services->have_posts()): $services->the_post(); 
                                  $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'full' ); 
                                  ?>
                                   <div class="service-item">
                                       <div class="service-item-content">
                                          <h2><?php echo get_the_title();?></h2>
                                          <?php the_content();?>
                                          <a class='btn btn-primary' href="<?php echo get_site_url();?>/contact">GET IN TOUCH</a>
                                      </div>
                                       <div class="service-item-thumb">
                                          <?php if( $image_url ){ ?>
                                             <img src="<?php echo $image_url;?>" alt="<?php echo get_the_title();?>" />
                                          <?php } ?>
                                       </div>
                                   </div>
                                <?php endwhile;
                                wp_reset_postdata(); ?>
                              </div>
		             <?php endif; ?>	
                          </div>
                    </div>
		</div>
            <?php } ?>
        <?php } ?>
<?php include 'page-bottom.php'; ?>
    </div>
<?php get_footer();
