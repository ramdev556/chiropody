<?php

    $phone_number = get_field('phone_number', 'option');
    $email_address = get_field('email_address', 'option');
    $slides = new WP_Query(array('post_type' => 'slides', 'order' => 'ASC', 'orderby' => 'menu_order'));
    if ($slides->have_posts()) :?>

        <div class="responsive-slider flexslider">
            <ul class="slides">
                <?php while ($slides->have_posts()) : $slides->the_post();
                    $image_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'full');
                    ?>
                    <li>
                        <div id="slide-<?php echo get_the_ID(); ?>" class="slide flexslider-slide background-image"
                             style="background-image:url(<?php echo $image_url; ?>)">
                            <div class="slider-content-wrap">
                                <?php global $post; ?>
                                <div class="container">
                                   <a class='site-logo' href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                      <img src="<?php echo $logo_src; ?>" alt="<?php echo get_bloginfo( 'name' ); ?>">
                                    </a>
                                  <h1><?php echo get_bloginfo( 'name' ); ?></h1>
                                  <h5><?php echo get_bloginfo( 'description' ); ?></h5>

                                   <div class='has-contact-btn'>
                                      <?php if( $phone_number ){ ?>
                                          <a href="skype:<?php echo $phone_number ;?>" class="disable-link">
                                            <i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone_number ;?>
                                          </a>
                                      <?php } ?>
                                      <?php if( $email_address ){ ?>
                                         <a href="mailto:<?php echo $email_address;?>">
                                           <i class="fa fa-envelope-o" aria-hidden="true"></i> Email Us
                                         </a>
                                      <?php } ?>
                                  </div>

                                </div>
                            </div>
                        </div>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>

    <?php endif;
    wp_reset_query();