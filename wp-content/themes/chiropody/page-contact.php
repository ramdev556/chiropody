<?php
/*
*
* Template Name: Contact Page
*
*
*/
get_header();
$email_address = get_field('email_address', 'option');
$phone_number = get_field('phone_number', 'option');
$sky_phone_number = str_replace(' ', '', $phone_number);
$office_address = get_field('office_address', 'option');
?>

    <div class="content-area">
        <?php if (have_posts()) { ?>
            <?php while (have_posts()) {
                the_post(); ?>
                    <div class="site-section pt-0 text-center">
                        <div class="container">
                         <h1 class="entry-title"><?php echo get_the_title();?></h1>
                                  <?php the_content();?>
                                  <div class='has-contact-btn has-contact-btn-page my-3 my-lg-5'>
                                      <?php if( $phone_number ){ ?>
                                             <a href="skype:<?php echo $phone_number ;?>" class="disable-link">
                                                <i class="fa fa-phone" aria-hidden="true"></i> <?php echo $phone_number ;?>
                                             </a>
                                       <?php } ?>
                                       <?php if( $email_address ){ ?>
                                             <a href="mailto:<?php echo $email_address;?>">
                                                 <i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $email_address ;?>
                                             </a>
                                       <?php } ?>
                                   </div>

                                  <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]');?>
                              </div>
                        </div>
                    </div>
            <?php } ?>
        <?php } ?>
    </div>
<?php get_footer();

