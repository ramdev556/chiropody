<?php
$footer_logo = get_field('footer_logo', 'option');
if ($footer_logo) {
    $footer_logo_src = $footer_logo;
} else {
    $footer_logo_src = get_bloginfo('stylesheet_directory') . '/images/footer-logo.png';
}
$copy_right_text = get_field('copy_right_text', 'option');
?>

</div>
<footer class="site-footer">

    <div class="site-footer-primary">
        <div class="container">
            <div class="footer-site-logo">
                <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                <img src="<?php echo $footer_logo_src; ?>" alt="<?php echo get_bloginfo( 'name' ); ?>">
                </a>
            </div>
        </div>
    </div>

    <div class="site-footer-secondary">
        <div class="container">
            <?php if ($copy_right_text) { ?>
              <p><?php echo $copy_right_text; ?></p>
            <?php } ?>
        </div>
    </div>


</footer>
<?php wp_footer(); ?>
</div>
</body>
</html>
