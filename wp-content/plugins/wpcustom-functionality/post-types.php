<?php 
function create_testimonial_post_type() {
    register_post_type( 'testimonial',
        array(
            'labels' => array(
                'name' => __( 'Testimonial' ),
				'add_new_item'        => __( 'Add New Testimonial' ),
                'add_new'      => __( 'Add New'),
                'edit_item'   => __( 'Edit Testimonial'), 
                'singular_name' => __( 'testimonial' )
            ),
            'public' => true,
            'has_archive' => true,
			'rewrite' => array('slug' => 'testimonial'),
            'supports' => array( 'title', 'editor' )
        )
    );
}
add_action( 'init', 'create_testimonial_post_type' );

function create_services_post_type() {
    register_post_type( 'services',
        array(
            'labels' => array(
                'name' => __( 'Services' ),
		'add_new_item'        => __( 'Add New Service' ),
                'add_new'             => __( 'Add New'),
                'edit_item'           => __( 'Edit Service'), 
                'singular_name' => __( 'services' )
            ),
            'public' => true,
            'has_archive' => true,
	    'rewrite' => array('slug' => 'services'),
            'supports' => array( 'title', 'editor', 'thumbnail' )
        )
    );
}
add_action( 'init', 'create_services_post_type' );

