<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'chiropody-podiatry' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>!Yt,[}>~&|iy j683nAhoQ^dDb!33^$u$-=t&@l`],,4QK79/_#UG.E%6bBl}*z' );
define( 'SECURE_AUTH_KEY',  ' :k?zio/Jw9loy!d~y pd~)@)%t|=~H2<1_(P7go&yB(Mjpr(M%n]vt{+K6f(&Ry' );
define( 'LOGGED_IN_KEY',    '7j!r|7p&15B}Lx<3wB7bYByMc/!tx-l!=R*~g1sjq$UJ{[d /P~xF4@^(*0A4Qfd' );
define( 'NONCE_KEY',        'c;UKOqSIcs]]:%qv0MN42&X*l8[(j]05767Sf$ScLH>~Z#HIr[@@N&$,B&L7qDFJ' );
define( 'AUTH_SALT',        '^/fz>jj@D:)|!}T/X{N.L|,jdq@]13g`m|~k,2-1&}{9JS%a?YUMMq3DR]#(rt*D' );
define( 'SECURE_AUTH_SALT', '-)|*[^5eU@.`U(U,j2k}J.kq.>U$e|C)npft{E$:<8]D;up[|2q#)aduy4gvZ(V_' );
define( 'LOGGED_IN_SALT',   'a</(r[0Rg:9qO/4U~^5<h#er^#^qOWT}D7bYrOmV%sgmqI4*FZ5KmZep[X!WR-xj' );
define( 'NONCE_SALT',       'G^%e<Ojr-5AV0{8@R_wHd{ a*+ZOjF^#{0L$J~hi_[u;H8;QJwH:KDohKdoq=E+h' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'cp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
